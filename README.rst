
proyecto_laferiacr
******************

Proyecto diseñado para administrar las tareas y documentación de la
`https://laferia.cr <https://laferia.cr>`_.

Código en:

`Gitlab <https://gitlab.com/laferiacr/proyecto_laferiacr>`_


Contenidos
**********

* `Descripción <#Descripción>`_
* `Uso <#Uso>`_
* `Requerimientos <#Requerimientos>`_
* `Compatibilidad <#Compatibilidad>`_
* `Licencia <#Licencia>`_
* `Enlaces <#Enlaces>`_
* `Autor <#Autor>`_

Descripción
***********

Proyecto diseñado para administrar las tareas y documentación de la
`laferiacr <https://laferia.cr>`_.



Uso
***

Descargar el script, dar permiso de ejecución y ejecutar:

::

   wget https://raw.githubusercontent.com/laferiacr/proyecto_laferiacr/master/proyecto_laferiacr.sh
   chmod +x proyecto_laferiacr.sh
   ./proyecto_laferiacr.sh -h



Requerimientos
**************

* Ruby 2.7.



Compatibilidad
**************

* Debian Buster.

* Ubuntu Bionic.



Licencia
********

MIT. Vea el archivo LICENSE para más detalles.



Enlaces
*******

* `Gitlab <https://gitlab.com/laferiacr/proyecto_laferiacr>`_



Autor
*****

.. image:: https://gitlab.com/laferiacr/proyecto_laferiacr/raw/master/img/author.png
   :alt: author

La feria CR.


