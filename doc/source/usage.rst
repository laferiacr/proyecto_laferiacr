Uso
------------------------------------------------------------------------------

Descargar el script, dar permiso de ejecución y ejecutar:

.. substitution-code-block:: bash

 wget https://raw.githubusercontent.com/|AUTHOR|/|PROJECT|/master/|PROJECT|.sh
 chmod +x |PROJECT|.sh
 ./|PROJECT|.sh -h