proyecto_laferiacr
==============================================================

Proyecto diseñado para administrar las tareas y documentación
de la `https://laferia.cr <https://laferia.cr>`_.

Código en:

|GITLAB_LINK|

Contenidos
==============================================================

.. toctree::

   description

   usage

   requirement

   compatibility

   license

   link

   author