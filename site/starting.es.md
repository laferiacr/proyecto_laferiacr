# Comenzando en su viaje con Openfoodnetwork | laferia.cr

Un "food hub" de alimentos en la plataforma Open Food Network es una tienda que vende artículos **(al público u otras empresas)** a más de un proveedor.

Es similar a mercado real en el que puede cultivar y suministrar papas por **el agricultor A**, pan horneado por el **señor B**, té y café producidos/importdos por la **señora C**, etc.

## ¿Quizás usted es el **agricultor A** y desea vender sus papas directamente en OFN (además de suministrar un "centro")?

En cuyo caso, debe registrarse como 'Tienda' en nuestra plataforma. Los pasos para abrir una tienda son muy similares a los de un centro (pero, en todo caso, ¡un poco más fáciles!).


Por otro lado, puede ser como el Sr. B: solo quiere usar OFN para suministrar su pan horneado al centro local, pero en realidad no tiene un escaparate en nuestra plataforma: un ‘perfil‘. En cuyo caso, los pasos que debe seguir para configurar son los más fáciles de todos.
Si alguna vez se atasca o no puede recordar lo que significa un término en particular en nuestra plataforma, consulte este pequeño glosario muy útil.
Registrar su negocio de alimentos en OFN es simple. Antes de comenzar, puede echar un vistazo a este video.
Desde el registro hasta el comercio en 5 simples pasos
Aquí nos centramos en los pasos que debe seguir como gerente de un OFN Hub para configurar su negocio en nuestra plataforma.
Configurar una tienda o perfil OFN es aún más fácil. Consulte los consejos y la orientación en nuestra Guía del usuario.

1. Registre su empresa
    * Registre su empresa y complete su configuración de empresa
2. Enumere sus productos.
    * Si fabrica productos para vender y gestiona ventas, puede agregar o importar sus productos a nuestra plataforma.
    * Alternativamente, si solo planea vender artículos hechos por otros en OFN, entonces debe asegurarse de tener la ‘relación‘ correcta con sus productores.
3. Configure un método de pago
    *Configure al menos un método de pago para que sus clientes puedan pagar contra reembolso (efectivo, cheque o tarjeta) o puede optar por el pago directo al momento de la compra a través de Stripe (tarjeta de crédito) o PayPal
4. Configure un método de envío
    * Ya sea que su empresa ofrezca un servicio de recolección solamente (también conocido como "hacer clic y recolectar") o entrega a domicilio, informe a sus clientes dónde y cuándo obtendrán los artículos que compraron configurando un método de envío.
5. Configure un ciclo de pedido
    * Tener un ciclo de pedido activo abrirá su tienda para que los clientes compren artículos.

*Felicidades! ¡Tu tienda ahora está en vivo!*
